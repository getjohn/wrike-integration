﻿<?php
include_once("/www/wrike/config.php");

function urlencode_rfc3986($string) {
	 return str_replace("%7E", "~", rawurlencode($string));
}

function getRequestToken() {
	global $consumer;
	
	$url = "https://www.wrike.com/rest/auth/request_token"; 
	$params = array(
		"oauth_callback" => '',
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_version" => "1.0"
	);
	
	
	$result = makeAPICall($url, $params);

	

	list($token, $token_secret) = explode("&", $result);
	list($label, $token) = explode("=", $token);
	list($label, $token_secret) = explode("=", $token_secret);

	// redirect to auth page
	$callback_url = "http://" . $_SERVER["HTTP_HOST"] . "/wrike/authorize.php?oauth_token_secret=" . $token_secret;
	
	header("Location: https://www.wrike.com/rest/auth/authorize?oauth_token=" . $token . "&oauth_callback=" . urlencode_rfc3986($callback_url));
}

function getAccessToken($token, $secret) {
	global $consumer;
	
	$url = "https://www.wrike.com/rest/auth/access_token";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
	);
	$result = makeAPICall($url, $params, $secret);


	
	list($access_token, $access_token_secret) = explode("&", $result);
	list($label, $access_token) = explode("=", $access_token);
	list($label, $access_token_secret) = explode("=", $access_token_secret);

	 $_url = "http://" . $_SERVER["HTTP_HOST"] . "/wrike/authorize.php?access_token=" . $access_token . "&access_token_secret=" . $access_token_secret;
	
	header("Location: " . $_url);
}

function getAddNormalTask($token, $secret, $extraParams=array()) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.task.add";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"title" => $extraParams['title']
	);
	$params['description'] = str_replace(array("\r\n", "\n", "\r"), "", nl2br($extraParams['description']));
	$params['parents'] = $extraParams['parents'];
	ksort($params);
	

	$result = makeAPICall($url, $params, $secret, "POST");
	$object = json_decode($result);

	return $object->task->id;
}


function getAddTask($token, $secret, $extraParams=array()) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.task.add";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"title" => $extraParams['zendesk_id'].' - '.$extraParams['zendesk_title'].' -'.$extraParams['zendesk_organisation'],
	);
	$params['description'] = str_replace(array("\r\n", "\n", "\r"), "", nl2br($extraParams['zendesk_description']));
	 $params['parents'] = '7281156';
	ksort($params);
	
	$result = makeAPICall($url, $params, $secret, "POST");
	$object = json_decode($result);


	if(is_object($object)){
		  mysql_query("INSERT INTO wrike SET wrike_task_id='".$object->task->id."', zendesk_id='".$extraParams['zendesk_id']."'") or die(mysql_error());
	}
	
}

function getTaskUpdates($token, $secret, $tlast){
	$url = "https://www.wrike.com/api/json/v2/wrike.task.updates";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"tLast" => $tlast
	);	 
	 
	ksort($params);
	
	$result = makeAPICall($url, $params, $secret, "POST");
	
	return $result;
}




function updateTask($token, $secret, $taskId){
	 global $consumer;
	$url = "https://www.wrike.com/api/json/v2/wrike.task.update";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"id" => $taskId,
		"title" => '',
	        "status" => "0"
	);	 
	 
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	
	
}

function updateTaskCreator($token, $secret, $taskId, $userId){
	 global $consumer;
	$url = "https://www.wrike.com/api/json/v2/wrike.task.update";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"id" => $taskId,
		"title" => '',
	        "status" => "0"
	);	 
	 
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	
	
}

function getUpdateComment($token, $secret, $extraParams=array()) {
	global $consumer;
	$q = mysql_query("SELECT * FROM wrike WHERE zendesk_id='".$extraParams['zendesk_id']."'");
	$r = mysql_fetch_assoc($q);
	
	$url = "https://www.wrike.com/api/json/v2/wrike.comment.add";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
	);
	
	$params['taskId'] = $r['wrike_task_id'];
	$params['text'] = str_replace(array("\r\n", "\n", "\r"), "", nl2br($extraParams['zendesk_description']));
	

	ksort($params);

	updateTask($token, $secret, $params['taskId']);
	$result = makeAPICall($url, $params, $secret, "POST");

}

function getCommentUpdates($token, $secret, $tlast){
	$url = "https://www.wrike.com/api/json/v2/wrike.comment.updates";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"tLast" => $tlast
	);	 
	 
	ksort($params);
	
	$result = makeAPICall($url, $params, $secret, "POST");
	
	return $result;
}

function getTaks($token, $secret) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.task.get";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0",
		"id" => "7281868"
	);
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");

	list($headers, $body) = explode("\r\n\r\n", $result, 2);
	
	return $body;
}

function getContacts($token, $secret) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.contacts.list";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
		);
	
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	$body = json_decode($result);
	
	return $body;
}


function getFolders($token, $secret) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.folder.tree";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
		);
	
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	$body = json_decode($result);
	
	return $body;
}

function getFoldersDetails($token, $secret, $folderid) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.folder.get";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
		);
	$params['id'] = $folderid;
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	$body = json_decode($result);
	
	return $body;
}



function getProfile($token, $secret) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.profile.get";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
		);
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");

	list($headers, $body) = explode("\r\n\r\n", $result, 2);
	
	return $body;
}

function makeAPICall($url, $authParams, $token_secret = "", $method = "GET") {
	global $secret;
	
	$query_string = "";
	foreach($authParams as $key => $value) {
		$query_string .= $key . "=" . urlencode_rfc3986($value) . "&";
	}
	$query_string = rtrim($query_string, "&");
	$key_parts = array(
		urlencode_rfc3986($secret), 
		$token_secret != ""? urlencode_rfc3986($token_secret): ""
	);
	$params = array(
		$method, 
		urlencode_rfc3986($url), 
		urlencode_rfc3986($query_string)
	);
	$base_string = implode("&", $params);
	$signature = base64_encode(hash_hmac("sha1", $base_string, implode("&", $key_parts), true));
	
	$authParams["oauth_signature"] = $signature;
	
	$query_string = "";
	foreach($authParams as $key => $value) {
		$query_string .= $key . "=" . urlencode_rfc3986($value) . "&";
	}
	$query_string = rtrim($query_string, "&");
	
	//echo $query_string;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt( $ch, CURLOPT_HEADER, false);
	if ($method == "GET") {
		curl_setopt($ch, CURLOPT_URL, $url . "?" . $query_string);
	}
	else {
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
	}
	$result = curl_exec($ch);
	curl_close($ch);
	
	return $result;
}


function getFilterTasks($token, $secret, $filter) {
	global $consumer;
	
	$url = "https://www.wrike.com/api/json/v2/wrike.task.filter";
	$params = array(
		"oauth_consumer_key" => $consumer,
		"oauth_nonce" => md5(microtime() . mt_rand()),
		"oauth_signature_method" => "HMAC-SHA1",
		"oauth_timestamp" => time(),
		"oauth_token" => $token,
		"oauth_version" => "1.0"
		);
	$params['fromStartDate'] = '2012-10-22';
	$params['toStartDate'] = '2012-10-26';
	//$params['statuses'] = '1';
	$params['fields'] = 'id,title,responsibleUsers,duration,startDate,dueDate,status';
	ksort($params);
	$result = makeAPICall($url, $params, $secret, "POST");
	$body = json_decode($result);
	
	return $body;
}


function curlWrap($url, $json, $action)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
	curl_setopt($ch, CURLOPT_URL, ZDURL.$url);
	curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
	switch($action){
		case "POST":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
		case "GET":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			break;
		case "PUT":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		default:
			break;
	}
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$output = curl_exec($ch);
	
	
	curl_close($ch);
	$decoded = json_decode($output);
	return $decoded;
}
?>