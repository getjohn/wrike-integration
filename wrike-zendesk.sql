-- phpMyAdmin SQL Dump
-- version 3.4.11
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 13 jan 2013 om 12:36
-- Serverversie: 5.1.54
-- PHP-Versie: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;



-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wrike`
--

CREATE TABLE IF NOT EXISTS `wrike` (
  `wrike_task_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zendesk_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `zendesk_id` (`zendesk_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wrike_tlast`
--

CREATE TABLE IF NOT EXISTS `wrike_tlast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tasks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wrike_tokens`
--

CREATE TABLE IF NOT EXISTS `wrike_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timeadded` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

