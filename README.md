# README #

See [this blog post](https://www.wrike.com/blog/08/12/2013/Wrike-and-Zendesk-integration-get-code-that-turns-your-tickets-tasks) for the article describing the code we forked.

This fork is to allow further integrations to be added, such as Mantis.